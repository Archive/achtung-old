# Note that this is NOT a relocatable package
%define ver      0.1
%define rel      SNAP
%define prefix   /usr

Summary: GNOME presentation program
Name: achtung
Version: %ver
Release: %rel
Copyright: LGPL
Group: X11/Programs
Source: ftp://ftp.gnome.org/pub/GNOME/sources/achtung/achtung-%{ver}.tar.gz
BuildRoot: /tmp/achtung-root
URL: http://www.gnome.org
Docdir: %{prefix}/doc

%description
Achtung is a presentation program for GNOME.

GNOME is the GNU Network Object Model Environment.  That's a fancy
name but really GNOME is a nice GUI desktop environment.  It makes
using your computer easy, powerful, and easy to configure.

%changelog

* Sat Jan 16 1999 Martin Baulig <martin@home-of-linux.org>

- Created this spec file.

%prep
%setup

%build
# Needed for snapshot releases.
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%prefix
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README
%{prefix}/bin/*
%{prefix}/share/*
