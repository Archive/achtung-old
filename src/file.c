/*
 * file.c: File loading and saving routines
 * 	   mostly stolen from gnumeric
 *
 * Author:
 *   Miguel de Icaza (miguel@kernel.org)
 *   George Lebl (jirka@5z.com)
 */
#include <config.h>
#include <gnome.h>
#include "atg-doc.h"
#include "main.h"
#include "callbacks.h"
#include "file.h"

static void
save_ok (GtkWidget *widget, GtkFileSelection *fsel)
{
	char *name = gtk_file_selection_get_filename (fsel);
	AtgDoc *doc = gtk_object_get_data(GTK_OBJECT(fsel),"doc");

	if (name [strlen (name)-1] != '/'){
		atg_doc_set_filename (doc, name,TRUE);
		/*FIXME: save the document here*/
		/*xmlSaveFile(doc->filename,doc->doc);*/
	}
	gtk_widget_destroy(GTK_WIDGET(fsel));
}

void
presentation_save_as (AtgDoc *doc)
{
	GtkFileSelection *fsel;
	
	g_return_if_fail (doc != NULL);
	g_return_if_fail (ATG_IS_DOC(doc));

	fsel = (GtkFileSelection *)gtk_file_selection_new (_("Save presentation as"));
	if (doc->real_file && doc->filename)
		gtk_file_selection_set_filename (fsel, doc->filename);

	gtk_object_set_data(GTK_OBJECT(fsel),"doc",doc);
	
	/* Connect the signals for Ok and Cancel */
	gtk_signal_connect (GTK_OBJECT (fsel->ok_button), "clicked",
			    GTK_SIGNAL_FUNC (save_ok), fsel);
	gtk_signal_connect_object (GTK_OBJECT (fsel->cancel_button), "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy), 
				   GTK_OBJECT(fsel));
	gtk_window_position (GTK_WINDOW (fsel), GTK_WIN_POS_MOUSE);

	/*if the presentation dies so do it's dialogs*/
	gtk_signal_connect_object_while_alive (GTK_OBJECT (doc), "destroy",
					       GTK_SIGNAL_FUNC (gtk_widget_destroy), 
					       GTK_OBJECT(fsel));

	gtk_widget_show (GTK_WIDGET (fsel));
}

void
presentation_save (AtgDoc *doc)
{
	g_return_if_fail (doc != NULL);
	g_return_if_fail (ATG_IS_DOC(doc));
	
	if (!doc->filename || !doc->real_file)
		presentation_save_as (doc);
	/*FIXME: save document here*/
	/*else
		xmlSaveFile(p->filename,p->doc);*/
}

static void
open_ok(GtkWidget *w, GtkFileSelection *fsel)
{
	char *name = gtk_file_selection_get_filename (fsel);

	if (name [strlen (name)-1] != '/')
		load_presentation(name);
	gtk_widget_destroy(GTK_WIDGET(fsel));
}

void
presentation_open (void)
{
	GtkFileSelection *fsel;
	
	fsel = (GtkFileSelection *) gtk_file_selection_new (_("Open presentation"));

	/* Connect the signals for Ok and Cancel */
	gtk_signal_connect (GTK_OBJECT (fsel->ok_button), "clicked",
			    GTK_SIGNAL_FUNC (open_ok), fsel);
	gtk_signal_connect_object (GTK_OBJECT (fsel->cancel_button), "clicked",
				   GTK_SIGNAL_FUNC (gtk_widget_destroy), 
				   GTK_OBJECT(fsel));
	gtk_window_position (GTK_WINDOW (fsel), GTK_WIN_POS_MOUSE);
	
	gtk_widget_show (GTK_WIDGET (fsel));
}
