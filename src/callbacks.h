#ifndef CALLBACKS_H
#define CALLBACKS_H

GnomeMDIChild * child_new_from_config (const gchar *string);

void new_cb(GtkWidget *w,gpointer data);
void save_cb(GtkWidget *w,gpointer data);
void save_as_cb(GtkWidget *w,gpointer data);
void close_cb(GtkWidget *w,gpointer data);
void about_cb(GtkWidget *w,gpointer data);
void quit_cb (GtkWidget *widget,gpointer data);
void add_edit_cb(GtkWidget *w,gpointer data);
void add_sorter_cb(GtkWidget *w,gpointer data);
void add_temporal_cb(GtkWidget *w,gpointer data);
void presentation_view_cb(GtkWidget *w,gpointer data);

void load_presentation(char *file);

gint remove_child_handler(GnomeMDI *mdi, GnomeMDIChild *child);

#endif CALLBACKS_H
