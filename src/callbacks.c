/*This file is for callbacks for the main window*/
#include <stdlib.h>
#include <stdio.h>

#include "config.h"
#include <gnome.h>

#include "main.h"
#include "file.h"
#include "atg-doc.h"
#include "atg-page.h"
#include "atg-container.h"
#include "atg-rectangle.h"

#include "edit.h"

#include "callbacks.h"

extern GnomeMDI *mdi;
extern GnomeClient *client;

static GnomeUIInfo child_menu[] = {
	GNOMEUIINFO_END
};

extern GnomeUIInfo edit_menu[];

static GtkWidget *
make_sorter_view(GnomeMDIChild *child, gpointer data)
{
	return gtk_label_new("\n\n\n\n\nSORTER VIEW GOES HERE\n\n\n\n\n");
}
static GtkWidget *
make_temporal_view(GnomeMDIChild *child, gpointer data)
{
	return gtk_label_new("\n\n\n\n\nTEMPORAL VIEW GOES HERE\n\n\n\n\n");
}

static gchar *
make_string(GnomeMDIChild *child, gpointer data)
{
	AtgDoc *doc = gtk_object_get_user_data(GTK_OBJECT(child));

	g_return_val_if_fail(doc!=NULL,g_strdup("error"));
	g_return_val_if_fail(ATG_IS_DOC(doc),g_strdup("error"));
	
	return g_strdup_printf ("%s:%s",(char *)data, doc->filename);
}

static GtkWidget *
child_set_label(GnomeMDIChild *child,
		GtkWidget *old_label,
		gpointer data)
{
	if(!old_label)
		return gtk_label_new(child->name);
	else {
		gtk_label_set_text(GTK_LABEL(old_label), child->name);
		return old_label;
	}
}

static void
generic_set_filename(AtgDoc *doc,
		     char *filename,
		     int real,
		     GnomeMDIChild *child)
{
	char *name;
	char *win_name = gtk_object_get_data(GTK_OBJECT(child),"win_name");
	name = g_strdup_printf(_("%s: %s"), win_name, filename?filename:"");
	gnome_mdi_child_set_name(child,name);
	g_free(name);
}

static GnomeMDIGenericChild *
add_new_generic_child(AtgDoc *doc, const char *filename,
		      char *win_name, char *id,
		      GnomeMDIChildViewCreator view_create_func,
		      GnomeUIInfo menu[])
{
	GnomeMDIGenericChild *child;
	gchar *name;

	name = g_strdup_printf(_("%s: %s"), win_name, filename);

	if((child = gnome_mdi_generic_child_new(name)) != NULL) {
		gnome_mdi_generic_child_set_view_creator(child, view_create_func,
							 NULL);
		gnome_mdi_child_set_menu_template(GNOME_MDI_CHILD(child), menu);
		gnome_mdi_generic_child_set_config_func_full(child, make_string,
							     NULL, g_strdup(id),
							     (GtkDestroyNotify)
							       g_free);
		gnome_mdi_generic_child_set_label_func(child, child_set_label, NULL);

		gtk_object_set_user_data(GTK_OBJECT(child), doc);
		gtk_object_set_data_full(GTK_OBJECT(child), "win_name",
					 g_strdup(win_name),
					 (GtkDestroyNotify)g_free);
		gtk_signal_connect_while_alive(GTK_OBJECT(doc),"set_filename",
					       GTK_SIGNAL_FUNC(generic_set_filename),
					       child,
					       GTK_OBJECT(child));
	} else {
		g_warning(_("Can't add window for %s"),filename);
	}
	g_free(name);

	return child;
}

static void
add_new_edit_child(AtgDoc *doc)
{
	GnomeMDIGenericChild *child;
	child = add_new_generic_child(doc, doc->filename,
				      _("Slide editor"),"EDIT",
				      make_edit_view,
				      edit_menu);
	gnome_mdi_add_child(mdi, GNOME_MDI_CHILD(child));
	gnome_mdi_add_view(mdi, GNOME_MDI_CHILD(child));
}
static void
add_new_sorter_child(AtgDoc *doc)
{
	GnomeMDIGenericChild *child;
	child = add_new_generic_child(doc, doc->filename,
				      _("Slide sorter"),"SORTER",
				      make_sorter_view,
				      child_menu);
	gnome_mdi_add_child(mdi, GNOME_MDI_CHILD(child));
	gnome_mdi_add_view(mdi, GNOME_MDI_CHILD(child));
}
static void
add_new_temporal_child(AtgDoc *doc)
{
	GnomeMDIGenericChild *child;
	child = add_new_generic_child(doc, doc->filename,
				      _("Temporal editor"),"TEMPORAL",
				      make_temporal_view,
				      child_menu);
	gnome_mdi_add_child(mdi, GNOME_MDI_CHILD(child));
	gnome_mdi_add_view(mdi, GNOME_MDI_CHILD(child));
}

GnomeMDIChild *
child_new_from_config (const gchar *string)
{
	GnomeMDIGenericChild *child = NULL;
	AtgDoc *doc = NULL;
	GList *li;
	char *s;
	if(!(s=strchr(string,':'))) {
		g_warning(_("Can't figure out how to load the window %s"),string);
		return NULL;
	}
	/*s is the filename now*/
	s++;

	/*see if we already have a window with such a presentation*/
	for(li=mdi->children;li!=NULL;li=g_list_next(li)) {
		doc = gtk_object_get_user_data(li->data);
		if(strcmp(doc->filename,s)==0)
			break;
	}
	/*if not found*/
	if(!doc) {
		GtkObject *o;
		o = atg_doc_new_from_file(s);
		if(!o) {
			GtkWidget *mbox;
			char buf[256];

			g_snprintf(buf,256,_("Can't open file '%s'"),s);
			mbox = gnome_message_box_new(buf,GNOME_MESSAGE_BOX_ERROR,
						     _("OK"),NULL);
			gtk_widget_show(mbox);
			return NULL;
		}
		doc = ATG_DOC(o);
	}
	
	if(strncmp(string,"EDIT:",sizeof("EDIT:")-1)==0) {
		child = add_new_generic_child(doc, string,
					      _("Slide editor"),"EDIT",
					      make_edit_view,
					      child_menu);
	} else if(strncmp(string,"SORTER:",sizeof("SORTER:")-1)==0) {
		child = add_new_generic_child(doc, string,
					      _("Slide sorter"),"SORTER",
					      make_sorter_view,
					      child_menu);
	} else if(strncmp(string,"TEMPORAL:",sizeof("TEMPORAL:")-1)==0) {
		child = add_new_generic_child(doc, string,
					      _("Temporal editor"),"TEMPORAL",
					      make_temporal_view,
					      child_menu);
	} else {
		g_warning(_("Can't figure out how to load the window %s"),string);
	}
	if(!child)
		return NULL;
	else
		return GNOME_MDI_CHILD (child);
}

void
new_cb(GtkWidget *w,gpointer data)
{
	static int pres_num=0;
	char buf[256];
	AtgDoc *doc;
	AtgObject *page;
	AtgObject *rect; /*bogus*/

	/*create empty dummy presentation*/
	doc = ATG_DOC(atg_doc_new());
	g_snprintf(buf,256,_("Presentation-%d"),++pres_num);
	atg_doc_set_filename(doc,buf,FALSE);
	
	page = atg_page_new();
	atg_doc_append_page(doc,ATG_PAGE(page));

	rect = atg_object_new(atg_rectangle_get_type(),
			      "x1",200,
			      "y1",200,
			      "x2",800,
			      "y2",400,
			      "color","green",
			      "outline_color","black",
			      "outline_width",5,
			      NULL);
	atg_container_add_object(ATG_CONTAINER(page),rect);
	
	add_new_edit_child(doc);
}

void
load_presentation(char *file)
{
	GtkObject *o;

	o = atg_doc_new_from_file(file);
	if(!o) {
		GtkWidget *mbox;
		char buf[256];

		g_snprintf(buf,256,_("Can't open file '%s'"),file);
		mbox = gnome_message_box_new(buf,GNOME_MESSAGE_BOX_ERROR,
					     _("OK"),NULL);
		gtk_widget_show(mbox);
		return;
	}

	add_new_edit_child(ATG_DOC(o));
}


void
save_cb(GtkWidget *w, gpointer data)
{
	GtkObject *child;
	AtgDoc *doc;
	
	child = GTK_OBJECT(gnome_mdi_get_active_child(mdi));
	if(!child) return;
	doc = gtk_object_get_user_data(child);
	
	g_return_if_fail(doc!=NULL);
	g_return_if_fail(ATG_IS_DOC(doc));
	
	presentation_save(doc);
}

void
save_as_cb(GtkWidget *w, gpointer data)
{
	GtkObject *child;
	AtgDoc *doc;
	
	child = GTK_OBJECT(gnome_mdi_get_active_child(mdi));
	if(!child) return;
	doc = gtk_object_get_user_data(child);
	
	g_return_if_fail(doc!=NULL);
	g_return_if_fail(ATG_IS_DOC(doc));
	
	presentation_save_as(doc);
}

void
close_cb(GtkWidget *w, gpointer data)
{
	GnomeMDIChild *child = gnome_mdi_get_active_child(mdi);
	if(!child) return;
	if(!child->views || g_list_length(child->views)<=1)
		gnome_mdi_remove_child(mdi,child, FALSE);
	else {
		GtkWidget *view = gnome_mdi_get_active_view(mdi);
		if(!view) return;
		gnome_mdi_remove_view(mdi,view, FALSE);
	}
}

void
about_cb(GtkWidget *w, gpointer data)
{
	GtkWidget *about;
	char *authors[] = {
	  "George Lebl (jirka@5z.com)",
	  "Tim P. Gerla (timg@means.net)",
	  "Erwin Rommel (erwin@afrika-korps.de)",
	  NULL
	  };

	about = gnome_about_new ( _("Achtung!"), VERSION,
			"(C) 1998-1999 the Free Software Foundation",
			(const gchar **)authors,
			_("The GNOME presentation program for creating those"
			  "extremely boring and dry presentations for those "
			  "extremely boring and dry meetings."),
			NULL);
	gtk_widget_show (about);
}

void
quit_cb (GtkWidget *widget, gpointer data)
{
	/* when the user wants to quit we try to remove all children and if we
	   succeed destroy the MDI. if TRUE was passed as the second (force)
	   argument, remove_child signal wouldn't be emmited.
	*/
	if(gnome_mdi_remove_all(mdi, FALSE))
		gtk_object_destroy(GTK_OBJECT(mdi));
}

void
add_edit_cb(GtkWidget *w, gpointer data)
{
	GtkObject *child;
	AtgDoc *doc;
	
	child = GTK_OBJECT(gnome_mdi_get_active_child(mdi));
	if(!child) return;
	doc = gtk_object_get_user_data(child);
	
	g_return_if_fail(doc!=NULL);
	g_return_if_fail(ATG_IS_DOC(doc));
	
	add_new_edit_child(doc);
}
void
add_sorter_cb(GtkWidget *w, gpointer data)
{
	GtkObject *child;
	AtgDoc *doc;
	
	child = GTK_OBJECT(gnome_mdi_get_active_child(mdi));
	if(!child) return;
	doc = gtk_object_get_user_data(child);
	
	g_return_if_fail(doc!=NULL);
	g_return_if_fail(ATG_IS_DOC(doc));
	
	add_new_sorter_child(doc);
}
void
add_temporal_cb(GtkWidget *w, gpointer data)
{
	GtkObject *child;
	AtgDoc *doc;
	
	child = GTK_OBJECT(gnome_mdi_get_active_child(mdi));
	if(!child) return;
	doc = gtk_object_get_user_data(child);
	
	g_return_if_fail(doc!=NULL);
	g_return_if_fail(ATG_IS_DOC(doc));
	
	add_new_temporal_child(doc);
}

void
presentation_view_cb(GtkWidget *w, gpointer data)
{
}

gint
remove_child_handler(GnomeMDI *mdi, GnomeMDIChild *child)
{
	/*this needs to ask if it's all ok right here*/
	return TRUE;
}
