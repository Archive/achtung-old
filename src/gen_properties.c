#include <gtk/gtk.h>
#include <gdk/gdkprivate.h>

#include <config.h>
#include <gnome.h>

#include "gen_properties.h"

static int 
properties_destroy(GtkWidget *widget, Properties *prop)
{
	GSList *li;
	
	for(li=prop->objs;li!=NULL;li=g_slist_next(li))  
		g_free(li->data);
	
	g_slist_free(prop->objs);

	g_hash_table_destroy(prop->props);
	
	g_free(prop);

	return FALSE;
}

/*if this properties was done by properties_run*/
static int 
properties_destroy_extra(GtkWidget *widget, GtkObject *o)
{
	gtk_object_set_data(o,"properties",NULL);

	return FALSE;
}

Properties *
properties_create(char *title)
{
	Properties *prop;
	
	prop = g_new(Properties,1);
	
	prop->dialog = gnome_property_box_new();
	gtk_window_set_policy(GTK_WINDOW(prop->dialog), FALSE, FALSE, TRUE);
	gtk_signal_connect(GTK_OBJECT(prop->dialog), "destroy",
			   GTK_SIGNAL_FUNC (properties_destroy), prop);
	gtk_window_set_title (GTK_WINDOW(prop->dialog),title);
	
	prop->props = g_hash_table_new(g_str_hash,g_str_equal);
	
	prop->objs = NULL;
	
	return prop;
}

void
properties_run(GtkObject *o, char *title, PropertiesFill fill_func)
{
	Properties *prop;
	
	prop = gtk_object_get_data(o,"properties");
	
	/*is a dialog already there?*/
	if(prop) {
		gdk_window_raise(prop->dialog->window);
		return;
	}
	
	prop = properties_create(title);

	gtk_object_set_data(o,"properties",prop);
	
	/*destroy the dialog if the object dies*/
	gtk_signal_connect_object(o, "destroy",
			   	  GTK_SIGNAL_FUNC (gtk_widget_destroy),
			   	  GTK_OBJECT(prop->dialog));
	gtk_signal_connect_object(GTK_OBJECT(prop->dialog), "destroy",
				  GTK_SIGNAL_FUNC (properties_destroy_extra),
				  o);
	(*fill_func)(prop,o);

	gtk_widget_show_all(prop->dialog);
}

GtkVBox *
properties_add_page(Properties *prop, char *title)
{
	GtkVBox * box;
	GtkNotebook *nbook;

	box = GTK_VBOX(gtk_vbox_new(FALSE,5));
	gtk_container_set_border_width(GTK_CONTAINER(box),5);

	nbook = GTK_NOTEBOOK(GNOME_PROPERTY_BOX(prop->dialog)->notebook);
	gtk_notebook_append_page(nbook,GTK_WIDGET(box),gtk_label_new(title));

	return box;
}

static void
add_list_to_box(GtkBox *box, GtkWidget *w, int strech, va_list args)
{
	while(w) {
		gtk_box_pack_start(box,w,strech,strech,0);
		w = va_arg(args,GtkWidget *);
		if(w)
			strech = va_arg(args,int);
	}
}

void
prop_page(Properties *prop, char *title, GtkWidget *w, int strech, ...)
{
	GtkBox *box;
	va_list args;

	box = GTK_BOX(properties_add_page(prop,title));

	va_start(args,strech);
	add_list_to_box(box,w,strech,args);
	va_end(args);

	gtk_widget_show_all(GTK_WIDGET(box));
}

GtkWidget *
prop_frame(Properties *prop, char *title, GtkWidget *w, int strech, ...)
{
	GtkWidget *frame;
	GtkBox *box;
	va_list args;

	frame = gtk_frame_new(title);
	box = GTK_BOX(gtk_vbox_new(FALSE,5));
	gtk_container_set_border_width(GTK_CONTAINER(box),5);
	gtk_container_add(GTK_CONTAINER(frame),GTK_WIDGET(box));

	va_start(args,strech);
	add_list_to_box(box,w,strech,args);
	va_end(args);

	return frame;
}

GtkWidget *
prop_vbox(Properties *prop, GtkWidget *w, int strech, ...)
{
	GtkWidget *box;
	va_list args;

	box = gtk_vbox_new(FALSE,5);

	va_start(args,strech);
	add_list_to_box(GTK_BOX(box),w,strech,args);
	va_end(args);

	return box;
}

GtkWidget *
prop_hbox(Properties *prop, GtkWidget *w, int strech, ...)
{
	GtkWidget *box;
	va_list args;

	box = gtk_hbox_new(FALSE,5);

	va_start(args,strech);
	add_list_to_box(GTK_BOX(box),w,strech,args);
	va_end(args);

	return box;
}

GtkWidget *
prop_entry(Properties *prop,
	   char *hash_key,
	   char *history_tag,
	   char *caption,
	   char *default_text)
{
	GtkWidget *w;
	GtkEntry *e;
	
	w = gnome_entry_new(history_tag);
	e = GTK_ENTRY(gnome_entry_gtk_entry(GNOME_ENTRY(w)));
	gtk_entry_set_text(e,default_text);
	gtk_signal_connect_object(GTK_OBJECT(e),"changed",
				  GTK_SIGNAL_FUNC(gnome_property_box_changed),
				  GTK_OBJECT(prop->dialog));
	hash_key = g_strdup(hash_key); /*make us a copy*/
	g_hash_table_insert(prop->props,hash_key,e);
	prop->objs = g_slist_prepend(prop->objs,hash_key);
	
	return w;
}

GtkWidget *
prop_plain_entry(Properties *prop,
		 char *hash_key,
		 char *caption,
		 char *default_text)
{
	GtkWidget *w;
	
	w = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(w),default_text);
	gtk_signal_connect_object(GTK_OBJECT(w),"changed",
				  GTK_SIGNAL_FUNC(gnome_property_box_changed),
				  GTK_OBJECT(prop->dialog));
	hash_key = g_strdup(hash_key); /*make us a copy*/
	g_hash_table_insert(prop->props,hash_key,w);
	prop->objs = g_slist_prepend(prop->objs,hash_key);
	
	return w;
}

GtkWidget *
prop_int_spinner(Properties *prop,
		 char *hash_key,
		 char *caption,
		 int value, int min, int max, int step, int page)
{
	GtkWidget *w;
	GtkAdjustment *adj;
	GtkWidget *box;

	box = gtk_hbox_new(FALSE,5);
	if(caption) {
		w = gtk_label_new(caption);
		gtk_box_pack_start(GTK_BOX(box),w,FALSE,FALSE,0);
	}
	adj = (GtkAdjustment *) gtk_adjustment_new (value,min,max,step,page,0.0);
	w = gtk_spin_button_new(adj,0,0);
	gtk_box_pack_start(GTK_BOX(box),w,TRUE,TRUE,0);

	gtk_signal_connect_object(GTK_OBJECT(adj),"value_changed",
				  GTK_SIGNAL_FUNC(gnome_property_box_changed),
				  GTK_OBJECT(prop->dialog));
	
	hash_key = g_strdup(hash_key); /*make us a copy*/
	g_hash_table_insert(prop->props,hash_key,adj);
	prop->objs = g_slist_prepend(prop->objs,hash_key);
	
	return box;
}

GtkWidget *
prop_color(Properties *prop,
	   char *hash_key,
	   char *caption,
	   GdkColor *def_color)
{
	GtkWidget *w;
	GtkWidget *box;

	box = gtk_hbox_new(FALSE,5);
	if(caption) {
		w = gtk_label_new(caption);
		gtk_box_pack_start(GTK_BOX(box),w,FALSE,FALSE,0);
	}
	w = gnome_color_picker_new();
	gnome_color_picker_set_i16(GNOME_COLOR_PICKER(w),
				   def_color->red,
				   def_color->green,
				   def_color->blue,
				   65535);
	gtk_box_pack_start(GTK_BOX(box),w,TRUE,TRUE,0);

	gtk_signal_connect_object(GTK_OBJECT(w),"color_set",
				  GTK_SIGNAL_FUNC(gnome_property_box_changed),
				  GTK_OBJECT(prop->dialog));
	
	hash_key = g_strdup(hash_key); /*make us a copy*/
	g_hash_table_insert(prop->props,hash_key,w);
	prop->objs = g_slist_prepend(prop->objs,hash_key);
	
	return box;
}

GtkWidget  *
prop_check_button(Properties *prop,
		  char *hash_key,
		  char *caption,
		  int value)
{
	GtkWidget *w;

	w = gtk_check_button_new_with_label(caption);
	gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(w),value);

	gtk_signal_connect_object(GTK_OBJECT(w),"toggled",
				  GTK_SIGNAL_FUNC(gnome_property_box_changed),
				  GTK_OBJECT(prop->dialog));
	
	hash_key = g_strdup(hash_key); /*make us a copy*/
	g_hash_table_insert(prop->props,hash_key,w);
	prop->objs = g_slist_prepend(prop->objs,hash_key);
	
	return w;
}

static void
font_ok_clicked_cb(GtkWidget *widget, GtkObject *o)
{
	GtkFontSelectionDialog *dlg;
	char *font;
	void (*updatefunc)(char *font, gpointer data);
	Properties *prop;
	
	dlg = gtk_object_get_data(o, "dialog");
	font = gtk_object_get_data(o, "font");
	updatefunc = gtk_object_get_data(o, "updatefunc");
	prop = gtk_object_get_data(o, "prop");

	g_free(font);
	font = gtk_font_selection_dialog_get_font_name(dlg);
	/*this is so I can bind this in font_color_entry*/
	if(updatefunc)
		(*updatefunc)(font, gtk_object_get_data(o, "updatefunc_data"));
	gnome_property_box_changed(GNOME_PROPERTY_BOX(prop->dialog));
	gtk_object_set_data(o,"font",font);
	gtk_object_set_data(o,"dialog",NULL);
	gtk_widget_destroy(GTK_WIDGET(dlg));
	gtk_signal_disconnect_by_data(o,dlg);
}

static void
font_cancel_clicked_cb(GtkWidget *widget, GtkObject *o)
{
	GtkWidget *dlg;
	dlg = gtk_object_get_data(o, "dialog");
	gtk_object_set_data(o,"dialog",NULL);
	gtk_widget_destroy(dlg);
	gtk_signal_disconnect_by_data(o,dlg);
}

static int
font_button_destroy(GtkWidget *w, gpointer data)
{
	GtkWidget *dlg;
	char *font;
	
	dlg = gtk_object_get_data(GTK_OBJECT(w), "dialog");
	font = gtk_object_get_data(GTK_OBJECT(w), "font");
	
	if(dlg)
		gtk_widget_destroy(dlg);
	if(font)
		g_free(font);
	
	return FALSE;
}

/*create the font dialog*/
static void
font_button_clicked(GtkWidget *w, char *caption)
{
	GtkWidget *dlg = gtk_object_get_data(GTK_OBJECT(w),"dialog");
	char *font = gtk_object_get_data(GTK_OBJECT(w),"font");
	if(dlg) {
		gdk_window_raise(dlg->window);
		return;
	}
	
	dlg = gtk_font_selection_dialog_new(caption);
	gtk_object_set_data(GTK_OBJECT(w),"dialog",dlg);
	gtk_signal_connect(GTK_OBJECT(w),"destroy",
			   GTK_SIGNAL_FUNC(font_button_destroy),dlg);

	gtk_font_selection_dialog_set_font_name(GTK_FONT_SELECTION_DIALOG(dlg),
						font);
	
	gtk_signal_connect(GTK_OBJECT(GTK_FONT_SELECTION_DIALOG(dlg)->ok_button),
			   "clicked",
			   GTK_SIGNAL_FUNC(font_ok_clicked_cb), w);
	gtk_signal_connect(GTK_OBJECT(GTK_FONT_SELECTION_DIALOG(dlg)->cancel_button),
			   "clicked",
			   GTK_SIGNAL_FUNC(font_cancel_clicked_cb), w);
	
	gtk_widget_show(dlg);
}

GtkWidget *
prop_font(Properties *prop,
	  char *hash_key,
	  char *caption,
	  char *default_font)
{
	GtkWidget *w;

	w = gtk_button_new_with_label(caption);
	
	gtk_object_set_data(GTK_OBJECT(w),"prop",prop);
	gtk_object_set_data(GTK_OBJECT(w),"font",g_strdup(default_font));
	
	hash_key = g_strdup(hash_key); /*make us a copy*/
	g_hash_table_insert(prop->props,hash_key,w);
	prop->objs = g_slist_prepend(prop->objs,hash_key);

	caption = g_strdup(caption); /*make us a copy*/
	prop->objs = g_slist_prepend(prop->objs,caption);

	gtk_signal_connect(GTK_OBJECT(w),"clicked",
			   GTK_SIGNAL_FUNC(font_button_clicked),
			   caption);
	
	return w;
}

static void
update_entry_style(char *font, gpointer data)
{
	GtkWidget *entry = data;
	GdkFont *f;
	GtkStyle *style;
	
	/* Trap errors with bad fonts (stolen from gimp:) */
	gdk_error_warnings = 0;
	gdk_error_code = 0;
	f = gdk_font_load (font);
	gdk_error_warnings = 1;

	if (gdk_error_code == -1) {
		g_message ("I'm sorry, but the font %s is corrupt.\n"
			   "Please ask the system adminstrator to replace it.",
			   font);
		return;
	}

	style = gtk_style_new ();
	gdk_font_unref (style->font);
	style->font = f;
	gdk_font_ref (f);

	gtk_widget_set_style (entry, style);
}

GtkWidget *
prop_font_color_entry(Properties *prop,
		      char *text_hash_key,
		      char *font_hash_key,
		      char *color_hash_key,
		      char *caption,
		      char *default_text,
		      char *default_font,
		      GdkColor *default_color)
{
	GtkWidget *fontsel;
	GtkWidget *textbox;

	fontsel = prop_font(prop,
			    font_hash_key,
			    _("Font"),
			    default_font);
	textbox = prop_plain_entry(prop,
				   text_hash_key,
				   NULL,
				   default_text);
	
	gtk_object_set_data(GTK_OBJECT(fontsel),"updatefunc",
			    update_entry_style);
	gtk_object_set_data(GTK_OBJECT(fontsel),"updatefunc_data",
			    textbox);
	
	update_entry_style(default_font,textbox);
	
	/*FIXME: do previews for colors as well*/
	
	return prop_frame(prop, caption,
			  prop_hbox(prop,
				    prop_color(prop,
					       color_hash_key,
					       NULL,
					       default_color), TRUE,
				    fontsel,TRUE,
				    NULL),FALSE,
			  textbox,TRUE,
			  NULL);
}


int
properties_get_bool(Properties* prop, char * hash_key)
{
	GtkToggleButton *tb;
	
	tb = g_hash_table_lookup(prop->props,hash_key);
	if(tb)
		return tb->active;
	return FALSE;
}

int
properties_get_int(Properties* prop, char * hash_key)
{
	GtkAdjustment *adj;
	
	adj = g_hash_table_lookup(prop->props,hash_key);
	if(adj)
		return (int)adj->value;
	return -1;
}

char *
properties_get_string(Properties* prop, char * hash_key)
{
	GtkEntry *entry;
	
	entry = g_hash_table_lookup(prop->props,hash_key);
	if(entry)
		return g_strdup(gtk_entry_get_text(entry));
	return NULL;
}

GdkColor
properties_get_color(Properties* prop, char * hash_key)
{
	GnomeColorPicker *gcolor;
	GdkColor col = {0,0,0,1};
	gushort r,g,b;
	
	gcolor = g_hash_table_lookup(prop->props,hash_key);
	if(gcolor) {
		gnome_color_picker_get_i16(gcolor,&r,&g,&b,NULL);
		col.red = r;
		col.green = g;
		col.blue = b;
	}
	return col;
}

char *
properties_get_font(Properties* prop, char * hash_key)
{
	GtkObject *o;
	
	o = g_hash_table_lookup(prop->props,hash_key);
	if(o)
		return g_strdup(gtk_object_get_data(o,"font"));
	return NULL;
}

void
properties_update_string(GtkObject *o, char *hash_key, char *string)
{
	Properties *prop = gtk_object_get_data(o,"properties");
	
	if(prop) {
		GtkEntry *entry;
		entry = g_hash_table_lookup(prop->props,hash_key);
		if(entry)
			gtk_entry_set_text(entry,string);
	}
}

void
properties_update_int(GtkObject *o, char *hash_key, int number)
{
	Properties *prop = gtk_object_get_data(o,"properties");
	
	if(prop) {
		GtkAdjustment *adj;
		adj = g_hash_table_lookup(prop->props,hash_key);
		if(adj)
			gtk_adjustment_set_value(adj,number);
	}
}

void
properties_update_color(GtkObject *o, char *hash_key, GdkColor *color)
{
	Properties *prop = gtk_object_get_data(o,"properties");
	
	if(prop) {
		GnomeColorPicker *gcolor;
		gcolor = g_hash_table_lookup(prop->props,hash_key);
		if(gcolor)
			gnome_color_picker_set_i16(gcolor,
						   color->red,
						   color->green,
						   color->blue,
						   65535);
	}
}

void
properties_update_font(GtkObject *o, char *hash_key, char *font)
{
	Properties *prop = gtk_object_get_data(o,"properties");
	
	if(prop) {
		GtkObject *o;
		o = g_hash_table_lookup(prop->props,hash_key);
		if(o) {
			GtkFontSelectionDialog *dlg;
			char *f;
			void (*updatefunc)(char *font, gpointer data);

			dlg = gtk_object_get_data(o, "dialog");
			f = gtk_object_get_data(o, "font");
			updatefunc = gtk_object_get_data(o, "updatefunc");

			if(dlg)
				gtk_font_selection_dialog_set_font_name(dlg,font);

			/*this is so I can bind this in font_color_entry*/
			if(updatefunc)
				(*updatefunc)(font, gtk_object_get_data(o, "updatefunc_data"));
			gtk_object_set_data(o,"font",g_strdup(font));
			g_free(f);
			gtk_object_set_data(o,"dialog",NULL);
		}
	}
}

void
properties_update_bool(GtkObject *o, char *hash_key, int value)
{
	Properties *prop = gtk_object_get_data(o,"properties");
	
	if(prop) {
		GtkToggleButton *tg;
		tg = g_hash_table_lookup(prop->props,hash_key);
		if(tg)
			gtk_toggle_button_set_state(tg,value);
	}
}
