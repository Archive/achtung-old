#ifndef GEN_PROPERTIES_H
#define GEN_PROPERTIES_H

/* a generic API for writing propperty dialogs of objects, could be useful
   in other places then achtung */

typedef struct _Properties Properties;
struct _Properties {
	GtkWidget *dialog;
	GHashTable *props;
	
	GSList *objs; /*pointers we need to free on destruction*/
};

/*"callback" for the properties_run function, it's when the
  dialog actually needs to get "filled"*/
typedef void (*PropertiesFill)(Properties *prop, GtkObject *o);

/*run this on an object to start it's properties or raise it's
  property dialog if it's already on screen somewhere, if the dialog
  is not yet created it will call fill_func*/
void properties_run(GtkObject *o, char *title, PropertiesFill fill_func);

/*if there is a properties open for this object, update the dialog */
void properties_update_string(GtkObject *o, char *hash_key, char *string);
void properties_update_int(GtkObject *o, char *hash_key, int number);
void properties_update_color(GtkObject *o, char *hash_key, GdkColor *color);
void properties_update_font(GtkObject *o, char *hash_key, char *font);
void properties_update_bool(GtkObject *o, char *hash_key, int value);

/*probably properties_run should be used on all objects, this would
  be if we want to handle what _run does ourselves*/
Properties *properties_create(char *title);

/*this is to be called in the fill function to make a new page, stuff
  will be packed into the vbox*/
GtkVBox *properties_add_page(Properties * prop, char *title);

/*here we have a beginning of a huge tree of a functioncall, this should
  make making dialogs simpler and cleaner, it will create the parent
  widget and pack all the widgets that it gets in the arguments to it, for example:

  prop_page(prop,"Foo",
	    prop_frame(prop,"Frame1",
		       prop_entry(prop,"text","bar","BlaBla","123"),TRUE,
		       prop_int_spinner(prop,"value","BlaBla",0,5,3),TRUE,
		       NULL), TRUE,
	    NULL);
  (a bit lispish I guess:)
*/
void prop_page(Properties *prop, char *title, GtkWidget *w, int strech, ...);
/*frame is fitted with a vbox*/
GtkWidget *prop_frame(Properties *prop, char *title, GtkWidget *w, int strech, ...);
GtkWidget *prop_vbox(Properties *prop, GtkWidget *w, int strech, ...);
GtkWidget *prop_hbox(Properties *prop, GtkWidget *w, int strech, ...);

GtkWidget *prop_entry(Properties *prop,
		      char *hash_key,
		      char *history_tag,
		      char *caption,
		      char *default_text);
GtkWidget *prop_plain_entry(Properties *prop,
			    char *hash_key,
			    char *caption,
			    char *default_text);
GtkWidget *prop_color(Properties *prop,
		      char *hash_key,
		      char *caption,
		      GdkColor *def_color);
GtkWidget *prop_int_spinner(Properties *prop,
			    char *hash_key,
			    char *caption,
			    int value, int min, int max, int step, int page);
GtkWidget *prop_check_button(Properties *prop,
			     char *hash_key,
			     char *caption,
			     int value);
GtkWidget *prop_font(Properties *prop,
		     char *hash_key,
		     char *caption,
		     char *default_font);
GtkWidget *prop_font_color_entry(Properties *prop,
				 char *text_hash_key,
				 char *font_hash_key,
				 char *color_hash_key,
				 char *caption,
				 char *default_text,
				 char *default_font,
				 GdkColor *default_color);

/*this is the way to retrieve values*/
int properties_get_bool(Properties* prop, char * hash_key);
int properties_get_int(Properties *prop, char *hash_key);
char * properties_get_string(Properties *prop, char *hash_key);
GdkColor properties_get_color(Properties *prop, char *hash_key);
char * properties_get_font(Properties *prop, char *hash_key);

#endif /* GEN_PROPERTIES_H */
