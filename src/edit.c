#include <stdlib.h>
#include <stdio.h>

#include "config.h"
#include <gnome.h>

#include "edit.h"
#include "atg-doc.h"
#include "edit-view.h"

#define SET_TOOL_PROC(name,tool)					\
static void								\
name(GtkWidget *w, GnomeMDIChild *ch)					\
{									\
	GtkWidget *wid = gnome_mdi_get_active_view(GNOME_MDI(ch->parent)); \
	edit_view_set_current_tool(EDIT_VIEW(wid),(tool));		\
}

SET_TOOL_PROC(set_point,EDIT_TOOL_POINT)
SET_TOOL_PROC(set_rectangle,EDIT_TOOL_RECTANGLE)
SET_TOOL_PROC(set_ellipse,EDIT_TOOL_ELLIPSE)
SET_TOOL_PROC(set_line,EDIT_TOOL_LINE)

GnomeUIInfo tools_menu[] = {
	GNOMEUIINFO_ITEM_NONE(N_("_Point"),
			      N_("Set current tool to point"),
			      set_point),
	GNOMEUIINFO_ITEM_NONE(N_("_Rectangle"),
			      N_("Set current tool to rectangle"),
			      set_rectangle),
	GNOMEUIINFO_ITEM_NONE(N_("_Ellipse"),
			      N_("Set current tool to ellipse"),
			      set_ellipse),
	GNOMEUIINFO_ITEM_NONE(N_("_Line"),
			      N_("Set current tool to line"),
			      set_line),
	GNOMEUIINFO_END
};

GnomeUIInfo edit_menu[] = {
	GNOMEUIINFO_SUBTREE(N_("_Tools"),tools_menu),
	GNOMEUIINFO_END
};

GtkWidget *
make_edit_view(GnomeMDIChild *child, gpointer data)
{
	AtgDoc *doc = gtk_object_get_user_data(GTK_OBJECT(child));
	return edit_view_new(doc);
}
