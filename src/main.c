/*
 * Achtung main.c
 *
 * Author:  George Lebl <jirka@5z.com>
 *
 * used some gnome-hello-mdi code
 *
 */

#include <config.h>
#include <gnome.h>

#include <stdio.h>

#include "atg-doc.h"

#include "main.h"

#include "file.h"
#include "callbacks.h"

static gboolean restarted = FALSE;

static struct poptOption prog_options[] = 
{
	POPT_AUTOHELP
	{NULL, '\0', 0, NULL, 0}
};

GnomeMDI *mdi;

GnomeClient *client;

static GnomeUIInfo file_menu[] = {
	GNOMEUIINFO_MENU_NEW_ITEM(N_("_New presentation..."),N_("Create a new presentation"),new_cb,NULL),
	GNOMEUIINFO_MENU_OPEN_ITEM(presentation_open,NULL),
	GNOMEUIINFO_MENU_SAVE_ITEM(save_cb,NULL),
	GNOMEUIINFO_MENU_SAVE_AS_ITEM(save_as_cb,NULL),
	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_CLOSE_ITEM(close_cb,NULL),
	GNOMEUIINFO_MENU_EXIT_ITEM(quit_cb,NULL),
	GNOMEUIINFO_END
};

GnomeUIInfo view_menu[] = {
	GNOMEUIINFO_ITEM_NONE(N_("Add slide editor window"),
			      N_("Add a new window with a slide editor for this presentation"),
			      add_edit_cb),
	GNOMEUIINFO_ITEM_NONE(N_("Add slide sorter window"),
			      N_("Add a new window with a slide sorter for this presentation"),
			      add_sorter_cb),
	GNOMEUIINFO_ITEM_NONE(N_("Add temporal editor window"),
			      N_("Add a new window with a temporal editor for this presentation"),
			      add_temporal_cb),
	GNOMEUIINFO_ITEM_NONE(N_("Presentation view"),
			      N_("View this presentation in full screen mode"),
			      presentation_view_cb),
	GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
	GNOMEUIINFO_HELP("achtung"),
	GNOMEUIINFO_MENU_ABOUT_ITEM(about_cb,NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo empty_menu[] = {
	GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE(file_menu),
	GNOMEUIINFO_MENU_VIEW_TREE(view_menu),
	GNOMEUIINFO_MENU_WINDOWS_TREE(empty_menu),
	GNOMEUIINFO_MENU_HELP_TREE(help_menu),
	GNOMEUIINFO_END
};


static void
cleanup_cb(GnomeMDI *mdi, gpointer data)
{
	/* on destruction of GnomeMDI we call gtk_main_quit(), since we have opened
	   no windows on our own and therefore our GUI is gone.
	*/
	gtk_main_quit();
}

static int
save_state (GnomeClient        *client,
	    gint                phase,
	    GnomeRestartStyle   save_style,
	    gint                shutdown,
	    GnomeInteractStyle  interact_style,
	    gint                fast,
	    gpointer            client_data)
{
	gchar *prefix= gnome_client_get_config_prefix (client);
	gchar *argv[]= { "rm", "-r", NULL };
  
	gnome_config_push_prefix (prefix);
  
	gnome_mdi_save_state (mdi, "MDI Session");
	
	gnome_config_set_bool ("General/restarted", TRUE);
  
	gnome_config_pop_prefix();
	gnome_config_sync();

	argv[2] = gnome_config_get_real_path (prefix);
	gnome_client_set_discard_command (client, 3, argv);

	argv[0] = (gchar*) client_data;
	gnome_client_set_clone_command (client, 1, argv);
	gnome_client_set_restart_command (client, 1, argv);
	
	return TRUE;
}

static void
app_created(GnomeMDI *mdi, GnomeApp *app)
{
	GtkWidget *bar;

	bar = gnome_appbar_new(FALSE, TRUE, GNOME_PREFERENCES_USER);
	gnome_app_set_statusbar(app, bar);
	gtk_widget_show(bar);

	gnome_app_install_menu_hints(app, gnome_mdi_get_menubar_info(app));
}

int
main(int argc, char *argv[])
{
	gboolean restart_ok = FALSE;

	gnome_init_with_popt_table("achtung",
				   VERSION, argc, argv,
				   prog_options, 0, NULL);
	
	/* session management init */
	client = gnome_master_client ();

	gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
			    GTK_SIGNAL_FUNC (save_state), argv[0]);

	mdi = GNOME_MDI(gnome_mdi_new("achtung", "GNOME Achtung"));

	gnome_mdi_set_menubar_template(mdi, main_menu);
	/*gnome_mdi_set_toolbar_template(mdi, toolbar_info);*/

	gnome_mdi_set_child_menu_path(mdi, GNOME_MENU_FILE_STRING);
	gnome_mdi_set_child_list_path(mdi, GNOME_MENU_WINDOWS_PATH);
  
	if (GNOME_CLIENT_CONNECTED (client)) {
		gnome_config_push_prefix (gnome_client_get_config_prefix (client));
		
		restarted = gnome_config_get_bool ("General/restarted=0");
		
		gnome_config_pop_prefix ();
	} else {
		restarted = FALSE;
	}	
	
	gtk_signal_connect(GTK_OBJECT(mdi), "destroy",
			   GTK_SIGNAL_FUNC(cleanup_cb), NULL);
	gtk_signal_connect(GTK_OBJECT(mdi), "remove_child",
			   GTK_SIGNAL_FUNC(remove_child_handler), NULL);
	gtk_signal_connect(GTK_OBJECT(mdi), "app_created",
			   GTK_SIGNAL_FUNC(app_created), NULL);
	
	gnome_mdi_set_mode(mdi, GNOME_MDI_DEFAULT_MODE);

	if (restarted) {
		gnome_config_push_prefix (gnome_client_get_config_prefix (client));

		restart_ok = gnome_mdi_restore_state (mdi, "MDI Session", child_new_from_config);

		gnome_config_pop_prefix ();
	} else {
		gnome_mdi_open_toplevel(mdi);
		new_cb(NULL,NULL);
	}

	gtk_main();
	
	return 0;
}
