#ifndef _FILE_H_
#define _FILE_H_

#include "atg-doc.h"

void presentation_save_as (AtgDoc *doc);
void presentation_save (AtgDoc *doc);
void presentation_open (void);

#endif
